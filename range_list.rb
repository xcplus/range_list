class RangeList
  def initialize
    @list = []
  end

  def add(arr)
    validate_range(arr)
    @list << arr and return @list if @list.empty?
    @list.unshift(arr) and return @list if @list.first.first > arr.last
    @list << arr and return @list if @list.last.last < arr.first
    res = []
    @list.each do |item|
      if res.empty? || res.last[1] < item[0]
        res << item
      else
        res.last[1] = [item[1], res.last[1]].max
      end
    end
    @list = res
  end

  def remove(arr)
    validate_range(arr)
    return @list if @list.empty? || @list.last.last < arr.first || @list.first.first > arr.last
    res = []
    @list.each do |item|
      res.push(item) and next if arr[1] < item[0] || arr[0] > item[1]
      if arr[0] > item[0] && arr[1] < item[1] # 当前区间包含删除区间
        res.push([item[0], arr[0]])
        res.push([arr[1], item[1]])
      elsif arr[0] < item[0] && arr[1] > item[1] # 删除区间包含当前区间
        next
      elsif arr[0] > item[0] && arr[1] > item[1] # 删除区间包含当前区间的右边界限
        res.push([item[0], arr[0]])
      elsif arr[0] <= item[0] && arr[1] < item[1] #删除区间包含当前区间的左边界限
        res.push([arr[1], item[1]])
      end
    end
    @list = res
  end
  
  def print
    @list.map { |arr| "[#{arr.first}, #{arr.last})" }.join(' ')
  end

  private

  # 验证参数
  def validate_range(arr)
    unless arr.is_a?(Array) && arr.size == 2 && arr.first <= arr.last
      raise ArgumentError, "Invalid range: #{arr}; must be an array of two numbers, first <= second; example: [1, 5]"
    end
  end

end
