require 'minitest/autorun'
require 'minitest/spec'
require_relative 'range_list'

describe RangeList do

  before do
    @rl = RangeList.new
  end

  describe "#add" do
    describe "the params must like [1,5] integer array" do
      it "should raise argument error with blank array params" do
        assert_raises(ArgumentError) { @rl.add([]) }
      end
      it "should raise argument error with other types params" do
        assert_raises(ArgumentError) { @rl.add(nil) }
        assert_raises(ArgumentError) { @rl.add(1) }
        assert_raises(ArgumentError) { @rl.add("string") }
        assert_raises(ArgumentError) { @rl.add(true) }
      end
    end

    # 添加第一个区间
    describe "add first interval" do
      it "should have one interval" do
        @rl.add([1, 5])
        assert_equal('[1, 5)', @rl.print)
      end
    end

    # 最左侧添加没有交集的区间
    describe "add an interval without intersection to the leftmost side" do
      it "should insert left" do
        @rl.add([6, 8])
        @rl.add([1, 5])
        assert_equal('[1, 5) [6, 8)', @rl.print)
      end
    end

    # 最右侧侧添加没有交集的区间
    describe "add an interval without intersection on the rightmost side" do
      it "should insert right" do
        @rl.add([6, 8])
        @rl.add([12, 15])
        assert_equal('[6, 8) [12, 15)', @rl.print)
      end
    end

    # 添加有交集的区间
    describe "add interval with intersection" do
      it "should merge intervals" do
        @rl.add([1, 8])
        @rl.add([12, 15])
        @rl.add([6, 8])
        @rl.add([5, 7])
        assert_equal('[1, 8) [12, 15)', @rl.print)
      end
    end

    # 添加有交集的单个数字区间
    describe "add a single number interval with intersection" do
      it "should merge intervals" do
        @rl.add([1, 8])
        @rl.add([8, 8])
        assert_equal('[1, 8)', @rl.print)
      end
    end
  end

  describe "#remove" do
    before do
      @rl = RangeList.new
      @rl.add([1, 5])
      @rl.add([10, 21])  
    end

    # 删除的区间在含有最小值区间的最左侧
    describe "deleted interval is on the leftmost side of the interval containing the minimum value" do
      it "should do nothing" do
        @rl.remove([0, 1])
        assert_equal('[1, 5) [10, 21)', @rl.print)
      end
    end

    # 删除的区间在含有最大值区间的最右侧
    describe "deleted interval is on the rightmost side of the interval containing the maximum value" do
      it "should do nothing" do
        @rl.remove([25, 30])
        assert_equal('[1, 5) [10, 21)', @rl.print)
      end
    end

    # 删除的区间在所有区间中都不相交
    describe "deleted intervals do not intersect in all intervals" do
      it "should do nothing" do
        @rl.remove([6, 7])
        assert_equal('[1, 5) [10, 21)', @rl.print)
      end
    end

    # 删除的区间和其中一个区间相交
    describe "deleted interval intersects one of the intervals" do
      it "should remove the intersection intervals" do
        @rl.remove([2, 7])
        assert_equal('[1, 2) [10, 21)', @rl.print)
      end
    end

    # 删除的区间包含其中一个区间
    describe "deleted interval contains one of the intervals" do
      it "should remove the intersection intervals" do
        @rl.remove([1, 7])
        assert_equal('[10, 21)', @rl.print)
      end
    end

    # 删除的区间和其中两个区间相交
    describe "deleted interval intersects two of them" do
      it "should remove the intersection intervals" do
        @rl.remove([2, 15])
        assert_equal('[1, 2) [15, 21)', @rl.print)
      end
    end
  end
end
